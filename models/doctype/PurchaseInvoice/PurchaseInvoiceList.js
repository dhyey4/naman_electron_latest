import { _ } from 'frappejs/utils';
import { getStatusColumn } from '../Transaction/Transaction';

export default {
  doctype: 'PurchaseInvoice',
  title: _('Quotations'),
  formRoute: name => `/edit/PurchaseInvoice/${name}`,
  columns: [
    'supplier',
    getStatusColumn('PurchaseInvoice'),
    'date',
    'grandTotal'
  ]
};
