const { getActions } = require('../Transaction/Transaction');
const InvoiceTemplate = require('../SalesInvoice/InvoiceTemplate.vue').default;

module.exports = {
  name: 'PurchaseInvoice',
  doctype: 'DocType',
  label: 'Purchase Invoice',
  printTemplate: InvoiceTemplate,
  isSingle: 0,
  isChild: 0,
  isSubmittable: 1,
  keywordFields: ['name', 'supplier'],
  showTitle: true,
  fields: [
    {
      label: 'Bill No',
      fieldname: 'name',
      fieldtype: 'Text'
    },
    {
      fieldname: 'date',
      label: 'Date',
      fieldtype: 'Date',
      default: new Date().toISOString().slice(0, 10)
    },
    {
      fieldname: 'supplier',
      label: 'Customer',
      fieldtype: 'Link',
      target: 'Party',
    },
    {
      fieldname: 'account',
      label: 'Title',
      fieldtype: 'Link',
      target: 'SalesInvoice'
    },
    {
      fieldname: 'currency',
      label: 'Supplier Currency',
      fieldtype: 'Text',
    },
    {
      fieldname: 'customer',
      label: 'Supplier Currency',
      fieldtype: 'Text',
    },
    {
      fieldname: 'exchangeRate',
      label: 'Exchange Rate',
      fieldtype: 'Text'
    },
    {
      fieldname: 'items',
      label: 'Items',
      fieldtype: 'Text',
    },
    {
      fieldname: 'netTotal',
      label: 'Net Total',
      fieldtype: 'Text',
    },
    {
      fieldname: 'baseNetTotal',
      label: 'Net Total (Company Currency)',
      fieldtype: 'Text',
    },
    {
      fieldname: 'taxes',
      label: 'Taxes',
      fieldtype: 'Text',
    },
    {
      fieldname: 'grandTotal',
      label: 'Grand Total',
      fieldtype: 'Text',
    },
    {
      fieldname: 'baseGrandTotal',
      label: 'Grand Total (Company Currency)',
      fieldtype: 'Text',
    },
    {
      fieldname: 'outstandingAmount',
      label: 'Outstanding Amount',
      
      fieldtype: 'Text',
    },
    {
      fieldname: 'terms',
      label: 'Terms',
      fieldtype: 'Text'
    }
  ],

  actions: getActions('PurchaseInvoice')
};
