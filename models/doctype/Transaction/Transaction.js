const frappe = require('frappejs');
const utils = require('../../../accounting/utils');
const { openQuickEdit } = require('@/utils');
const Badge = require('@/components/Badge').default;

module.exports = {
  getStatusColumn() {
    return {
      label: 'Status',
      fieldname: 'status',
      fieldtype: 'Select',
      render(doc) {
        let status = 'Unpaid';
        let color = 'orange';
        if (!doc.submitted) {
          status = 'Draft';
          color = 'gray';
        }
        if (doc.submitted === 1 && doc.outstandingAmount === 0.0) {
          status = 'Paid';
          color = 'green';
        }
        return {
          template: `<Badge class="text-xs" color="${color}">${status}</Badge>`,
          components: { Badge }
        };
      }
    };
  },
  getActions(doctype) {
    return [
      {
        label: 'Print',
        condition: doc => doc.submitted,
        action(doc, router) {
          router.push(`/print/${doc.doctype}/${doc.name}`);
        }
      },
      utils.ledgerLink
    ];
  }
};
