const frappe = require('frappejs');
let { _ } = require('frappejs/utils');

module.exports = {
  name: 'Party',
  label: 'Customers',
  keywordFields: ['name'],
  fields: [
    {
      fieldname: 'name',
      label: 'Name',
      fieldtype: 'Data',
      required: 1,
      placeholder: 'Full Name'
    },
    {
      fieldname: 'email',
      label: 'Email',
      fieldtype: 'Data',
      placeholder: 'john@doe.com',
      validate: {
        type: 'email'
      }
    },
    {
      fieldname: 'phone',
      label: 'Phone',
      fieldtype: 'Data',
      placeholder: 'Phone',
      validate: {
        type: 'phone'
      }
    },
    {
      fieldname: 'address',
      label: 'Address',
      fieldtype: 'Link',
      target: 'Address',
      placeholder: _('Click to create'),
      inline: true
    },
    {
      fieldname: 'nameOnQuo',
      label: 'Name On Qua.',
      fieldtype: 'Data',
      placeholder: 'Name'
    },
  ],

  quickEditFields: ['email', 'phone', 'address', 'nameOnQuo']
};
