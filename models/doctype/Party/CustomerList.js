import { _ } from 'frappejs/utils';

export default {
  doctype: 'Customer',
  title: _('Customers'),
  columns: ['name', 'email', 'address', 'nameOnQuo']
};
