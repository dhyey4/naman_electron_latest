import { _ } from 'frappejs/utils';

export default {
  doctype: 'Party',
  columns: ['name', 'phone', 'email', 'nameOnQuo']
};
