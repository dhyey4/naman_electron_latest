const { getActions } = require('../Transaction/Transaction');
const InvoiceTemplate = require('./InvoiceTemplate.vue').default;

module.exports = {
  name: 'SalesInvoice',
  label: 'Sales Invoice',
  doctype: 'DocType',
  documentClass: require('./SalesInvoiceDocument'),
  printTemplate: InvoiceTemplate,
  isSingle: 0,
  isChild: 0,
  isSubmittable: 1,
  titleField: 'invoiceId',
  keywordFields: ['customer'],
  settings: 'SalesInvoiceSettings',
  fields: [
    {
      fieldname: 'invoiceId',
      label: 'Title',
      fieldtype: 'Data'
    },
    {
      fieldname: 'quentity',
      label: 'Quentity',
      fieldtype: 'Data'
    },
    {
      fieldname: 'title',
      label: 'Title',
      fieldtype: 'Data'
    },
    {
      fieldname: 'titleAuto',
      label: 'Title',
      fieldtype: 'Link',
      target: 'SalesInvoice'
    },
    {
      fieldname: 'customer',
      label: 'Customer',
      fieldtype: 'Link',
      target: 'Party',
      required: 1
    },
    {
      fieldname: 'data',
      label: 'Data',
      fieldtype: 'Link',
      target: 'Party'
    },
    {
      fieldname: 'allData',
      label: 'Item Data',
      fieldtype: 'Text',
    },
    {
      fieldname: 'grandTotal',
      label: 'Grand Total',
      fieldtype: 'Data',
    },
    {
      fieldname: 'perBoxTotal',
      label: 'Per Box Total',
      fieldtype: 'Data',
    },
  ],

  actions: getActions('SalesInvoice')
};
