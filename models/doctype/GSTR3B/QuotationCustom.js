import { _ } from 'frappejs/utils';

export default {
  doctype: 'QuotationCustom',
  title: _('Quotation'),
  formRoute: name => `/edit/QuotationCustom/${name}`,
 
  columns: ['id', 'supplier', 'date']
};
